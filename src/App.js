import React from "react";
import Join from "./components/join/Join";
import Chat from "./components/chat/Chat";
import VideoConference from "./components/jitsi/VideoConference";

import { BrowserRouter as Router, Route } from "react-router-dom";
function App() {
  return (
    <Router>
      <Route path="/" component={Join} exact />
      <Route path="/chat" component={Chat} />
      <Route path="/video" component={VideoConference} />
      <Route path="/session/:session_id/:username" component={Join}  />
    </Router>
  );
}

export default App;
