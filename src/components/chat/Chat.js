import React, { useState, useEffect,setState } from "react";
import queryString from "query-string";
import io from "socket.io-client";
import "./chat.css";
import InfoBar from "../infoBar/InfoBar";
import Input from "../input/Input";
import Messages from "../messages/Messages";
import Bottom from "../bottomBar/Bottom";
import VideoConference from "../jitsi/VideoConference"
import Box from '@material-ui/core/Box';
// import Dialog from "../dialog/Dialog"
import { Link } from "react-router-dom";

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { deepOrange, deepPurple } from '@material-ui/core/colors';
import { CenterFocusStrong } from "@material-ui/icons";





const stylesDialog = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    color: theme.palette.primary.light
  },
  closeButton: {
    position: 'absolute',
    left: theme.spacing(32),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});



let socket;
const Chat = ({ location }) => {
  const [name, setName] = useState("");
  const [room, setRoom] = useState("");
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [video, setVideo] = React.useState(false);


  const DialogTitle = withStyles(stylesDialog)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
         {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
        <Typography variant="h6">{children}</Typography>
       
      </MuiDialogTitle>
    );
  });
  
  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(1),
    },
  }))(MuiDialogContent);
  
  const DialogActions = withStyles((theme) => ({
    root: {
      margin: 0,
      padding:theme.spacing(1),
      alignItems: CenterFocusStrong,
      padding: theme.spacing(1),
    },
  }))(MuiDialogActions);

 
  





  

  useEffect(() => {

    const { name, room } = queryString.parse(location.search);
    console.log(name)
    console.log(room)
  //   fetch(`http://13.233.222.4:9000/session?session_id=${room}&&username=${name}`)
  //   .then(res => res.json())
  //   .then(res => {
  //     console.log(res)
  //     // setRoom({ room })
  //     // setName({name: res.username })
  // }
  //    )
  //  .catch(() => this.setState({ hasErrors: true }))
    const ENDPOINT = "http://13.233.222.4";
    //console.log(data.name);
    setName(name);
    setRoom(room);
    socket = io(ENDPOINT);
  
    socket.emit("join", { name, room }, (err) => {
      if (err) console.log(err);
    });
   
  }, [location.search]);

//Dialog box open
  const handleClickOpen = () => {
    setOpen(true);
  };
//Dialog box close
  const handleClose = () => {
    setOpen(false);
  };


  
  
  //receive message socket
  useEffect(() => {
    
    socket.on("message", (message) => {
     console.log("messageChat",message)
    
   //Sessionid from app
  
     if (message.text==="video_call_initiated") {
       console.log("wow")
       handleClickOpen();
    
}
    
    setMessages([...messages, message]);
    });
  }, [messages]);




//send message socket
  const sendMessage = (e) => {
    e.preventDefault();
   let format = Object.assign(
      {
        _id:Math.round(Math.random() * 1000000),
        name: name,
        text: message,
        room: room,
        createdAt: new Date()
      })
    console.log("beforesend", format);
    socket.emit("sendMessage", format, () => {
     setMessage("");
    });
  };
//message log


 console.log("Chatfile", message)


  return (
   
      <div className="container">
        <InfoBar room={room} />
        <Messages messages={messages} name={name} />
        <Box className="box"  p={2}>
        <Bottom />
        <Dialog  onClose={handleClose}  open={open} >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {"Video call has been initiated"}
        </DialogTitle>
        <DialogContent>
         
          <Typography gutterBottom>
         Do you want to join the call
          </Typography>
          
        </DialogContent>
        <DialogActions>
        <Link
          onClick={(e) => {
           
              // e.preventDefault();
          }}
          to={`/video?name=${name}&room=${room}`}
        >
          <Button autoFocus onClick={handleClose} color="primary">
         
            Yes
          </Button>
          </Link>
          <Button autoFocus onClick={handleClose} color="primary">
          No
         
          </Button>
          
        </DialogActions>
      </Dialog>
     
        
        <br />
       <Input
          sendMessage={sendMessage}
          setMessage={setMessage}
          message={message}
           />
        </Box>
        
      </div>
      
    
   
  );
};





export default Chat;
