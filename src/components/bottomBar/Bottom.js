import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";

import Fab from "@material-ui/core/Fab";
import video from "../../assets/Images/video.png";
import user from "../../assets/Images/user-outline.png";
import file from "../../assets/Images/file.png";
import call from "../../assets/Images/call.png";

// import "./bottom.css";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    },
    width: "100%",
    backgroundColor:"#4782DE",
    justifyContent:"space-evenly",
    marginBottom:"20px"
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },

  
}));



const useStyle = makeStyles ({
  root: {
    
    backgroundColor:"rgba(0,0,0,0.5)",
    
  },
  
  
});



export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const fabclass=useStyle();
  
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
      >
<Fab className={fabclass.root} aria-label="edit">
      {/* <BottomNavigationAction  icon={<VideocamIcon />} /> */}
      {/* <VideocamIcon /> */}
      <img src={video} alt="some" width="20px"/>
      </Fab>
      <Fab className={fabclass.root} aria-label="edit">
      {/* <BottomNavigationAction  icon={<VideocamIcon />} /> */}
      {/* <VideocamIcon /> */}
      <img src={call} alt="some" width="20px"/>
      </Fab>
      <Fab className={fabclass.root} aria-label="edit">
      {/* <BottomNavigationAction  icon={<VideocamIcon />} /> */}
      {/* <VideocamIcon /> */}
      <img src={file} alt="some" width="20px"/>
      </Fab>
      <Fab className={fabclass.root} aria-label="edit">
      {/* <BottomNavigationAction  icon={<VideocamIcon />} /> */}
      {/* <VideocamIcon /> */}
      <img src={user} alt="some" width="20px"/>
      </Fab>
     
    

    </BottomNavigation>
  );
}
