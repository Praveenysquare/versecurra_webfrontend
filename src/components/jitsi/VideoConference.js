  
import React, { useState, useEffect } from 'react';
import queryString from "query-string";
// import {roomName} from "../chat/Chat"

function VideoConference({ location }) {

    
    
    // console.log(roomName)

  const jitsiContainerId = 'jitsi-container-id'
  const [jitsi, setJitsi] = useState({})

  const loadJitsiScript = () => {
    let resolveLoadJitsiScriptPromise = null;

    const loadJitsiScriptPromise = new Promise(resolve => {
      resolveLoadJitsiScriptPromise = resolve;
    });

    const script = document.createElement("script");
    script.src = "https://jitsi-poc.securra.com/external_api.js";
    script.async = true;
    script.onload = () => resolveLoadJitsiScriptPromise(true);
    document.body.appendChild(script);

    return loadJitsiScriptPromise;
  }

  const initJitsi = async () => {
    if (!window.JitsiMeetExternalAPI) {
      await loadJitsiScript();
    }

    const { name, room } = queryString.parse(location.search);

    const _jitsi = new window.JitsiMeetExternalAPI("jitsi-poc.securra.com", {

      roomName: room,  
      parentNode: document.getElementById(jitsiContainerId),
      displayName:name

    });

    setJitsi(_jitsi);

    _jitsi.addEventListener('videoConferenceLeft', () => {
        console.log('Local User left');
       
       });
  }

  useEffect(() => {
    initJitsi()
    return () => jitsi?.dispose?.()
  }, [])

  return <div id={jitsiContainerId} style={{ height: '100vh', width: "100%" }} />;
}

export default VideoConference;