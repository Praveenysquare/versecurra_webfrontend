import React from "react";
import "./input.css";

import { makeStyles } from "@material-ui/core/styles";
import NearMeOutlined from '@material-ui/icons/NearMeOutlined'
import Fab from "@material-ui/core/Fab";
import send from "../../assets/Images/send.png";


const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    },
  
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },

  
}));


const useStyle = makeStyles ({
  root: {
    
    backgroundColor:"rgba(0,0,0,0.5)",
    
  },
  
  
});

const Input = ({ message, setMessage, sendMessage }) => {
  const fabclass=useStyle();
  const classes = useStyles();
  
  return (
    <form className="form">
      <br />
      <input
        type="text"
        className="input"
        placeholder="send a message"
        value={message}
        onChange={({ target: { value } }) => setMessage(value)}
        onKeyPress={(e) => (e.key === "Enter" ? sendMessage(e) : null)}
      />
    
        
          <Fab className={classes.root}  onClick={
        (e) =>  sendMessage(e)}>

       <img src={send} alt="some" width="15px"/>
        </Fab>
        
  
    </form>
  );
};
export default Input;
