import React from "react";
import "./message.css";
import ReactEmoji from "react-emoji";
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { deepOrange, deepPurple } from '@material-ui/core/colors';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  orange: {
    color: theme.palette.getContrastText("#9fede9"),
    backgroundColor: "#9fede9",
  },
  purple: {
    color: theme.palette.getContrastText(deepPurple[500]),
    backgroundColor: deepPurple[500],
  },
}));


const Message = ({ message, name}) => {
  console.log("messageName", name)
  console.log("message", message)
  let isSentByCurrentUser = false;
  const classes = useStyles();
  if (name.trim().toLowerCase() === message.user.trim().toLowerCase())
    isSentByCurrentUser = true;
  return isSentByCurrentUser ? (
    <div className="messageContainer justifyEnd">
      <p className="sentText pr-10">{name}</p>
      <div className="messageBoxSender backgroundLight">
        <p className="messageText colorDark">
          {ReactEmoji.emojify(message.text)}

        </p>
      </div>
      <div className={classes.root}>
      
  <Avatar className={classes.orange}>{name.charAt(0).toUpperCase()}</Avatar>
      
    </div>
    </div>
  ) : (
    <div className="messageContainer justifyStart">
       <div className={classes.root}>
      
      <Avatar className={classes.purple}>{message.user.charAt(0).toUpperCase()}</Avatar>
          
        </div>
      <div className="messageBox backgroundLight">
        <p className="messageText colorDark">
          {ReactEmoji.emojify(message.text)}

        </p>
      </div>
      <p className="sentText pl-10">{message.user}</p>
    </div>
  );
};

export default Message;
