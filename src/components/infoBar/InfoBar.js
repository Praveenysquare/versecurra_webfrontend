import React from "react";
import "./infoBar.css";
import closeIcon from "../../icons/applogo.png";
// import onlineIcon from "../../icons/companyLogo.png";

const InfoBar = ({ room }) => {
  return (
    <div className="infoBar">
      <div className="leftInnerContainer">
        <img className="onlineIcon" src={closeIcon} alt="online image" width='140' />
        {/* <h3>{room}</h3> */}
      </div>
      {/* <div className="rightInnerContainer">
        <a href="/">
          <img src={onlineIcon} alt="close image" />
        </a>
      </div> */}
    </div>
  );
};

export default InfoBar;
